<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['manage/auth'] = 'auth/auth_set/login';
$route['manage/([a-zA-Z_-]+)'] = '$1/$1_set';
$route['manage/auth/(:any)'] = 'auth/auth_set/$1';
$route['manage/([a-zA-Z_-]+)/(:any)'] = '$1/$1_set/$2';
$route['manage/(:any)/edit/(:num)'] = "$1/$1_set/add/$2";
$route['manage/(:any)/(:any)/edit/(:num)'] = "$1/$1_set/add_$2/$3";
$route['manage/(:any)/(:any)/(:num)/(:num)'] = "$1/$1_set/$2/$3/$4";
$route['manage/(:any)/(:any)/(:num)/(:num)/(:num)'] = "$1/$1_set/$2/$3/$4/$5";
$route['manage/(:any)/(:any)/(:num)/(:num)/(:num)/(:num)'] = "$1/$1_set/$2/$3/$4/$5/$6";
$route['manage/(:any)/(:any)/(:num)'] = "$1/$1_set/$2/$3";
$route['manage/(:any)/(:any)/(:any)'] = "$1/$1_set/$3_$2";
$route['manage'] = "dashboard/Dashboard_set";

$route['student/auth'] = 'student/auth_student/login';
$route['student/([a-zA-Z_-]+)'] = '$1/$1_student';
$route['student/auth/(:any)'] = 'student/auth_student/$1';
$route['student/([a-zA-Z_-]+)/(:any)'] = '$1/$1_student/$2';
$route['student/(:any)/edit/(:num)'] = "$1/$1_student/add/$2";
$route['student/(:any)/(:any)/edit/(:num)'] = "$1/$1_student/add_$2/$3";
$route['student/(:any)/(:any)/(:num)/(:num)'] = "$1/$1_student/$2/$3/$4";
$route['student/(:any)/(:any)/(:num)/(:num)/(:num)'] = "$1/$1_student/$2/$3/$4/$5";
$route['student/(:any)/(:any)/(:num)'] = "$1/$1_student/$2/$3";
$route['student/(:any)/(:any)/(:any)'] = "$1/$1_student/$3_$2";
$route['student'] = "dashboard/Dashboard_student";


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
