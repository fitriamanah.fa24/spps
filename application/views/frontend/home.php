<?php
error_reporting(0);
?>
<section class="content">
  <div class="row"> 
    <div class="col-md-12">
      <div class="box box-info box-solid" style="border: 1px solid #2ABB9B !important;">
        <div class="box-header backg with-border">
          <h3 class="box-title">Cek Data Pembayaran Siswa</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

          <?php echo form_open(current_url(), array('class' => 'form-horizontal', 'method' => 'get')) ?>

          <div class="form-group">            
            <label for="" class="col-sm-2 control-label">Tahun Ajaran</label>
            <div class="col-sm-2">
              <select class="form-control" name="n">
                <!-- <option value="">-- Tahun Ajaran --</option> -->
                <?php foreach ($period as $row): ?>
                  <option <?php echo (isset($f['n']) AND $f['n'] == $row['period_id']) ? 'selected' : '' ?> value="<?php echo $row['period_id'] ?>"><?php echo $row['period_start'].'/'.$row['period_end'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <label for="" class="col-sm-2 control-label">Cari Siswa</label>
            
            <div class="col-sm-2">
              <input type="text" autofocus name="r" <?php echo (isset($f['r'])) ? 'placeholder="'.$f['r'].'"' : 'placeholder="NIS Siswa"' ?> class="form-control" required>
            </div>

            <div class="col-sm-4">
              <button type="submit" class="btn btn-success" id="openPasswordModal">
				      <i class="fa fa-search"> </i> Cari Siswa</button>
            </div>
          </div>
        </form>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
    <?php if ($f) { ?>

    <div class="row">
      <div class="col-md-6">
        <div class="box box-info box-solid" style="border: 1px solid #2ABB9B !important;">
          <div class="box-header backg with-border">
            <h3 class="box-title">Informasi Siswa</h3>
          </div><!-- /.box-header -->

          <div class="box-body">
            <table class="table table-striped">
              <tbody>

			  <?php foreach ($siswa as $row): ?>
				<?php if (isset($f['n']) AND $f['r'] == $row['student_nis']) { ?> 
					<?php if (!empty($row['student_img'])) { ?>
						<center>	<img src="<?php echo upload_url('student/'.$row['student_img']) ?>" class="img-thumbnail img-responsive">
							<?php } else { ?>
								<img src="<?php echo media_url('img/user.png') ?>" class="img-thumbnail img-responsive">
							<?php } 
					} ?>
				<hr>
				<?php endforeach; ?>
                <tr>
                  <td width="200">Tahun Ajaran</td><td width="4">:</td>
                  <?php foreach ($period as $row): ?>
                    <?php echo (isset($f['n']) AND $f['n'] == $row['period_id']) ? 
                    '<td><strong>'.$row['period_start'].'/'.$row['period_end'] .'<strong></td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <tr>
                    <td>NIS</td>
                    <td>:</td>
                    <?php foreach ($siswa as $row): ?>
                      <?php echo (isset($f['n']) AND $f['r'] == $row['student_nis']) ? 
                      '<td>'.$row['student_nis'].'</td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <tr>
                    <td>Nama Siswa</td>
                    <td>:</td>
                    <?php foreach ($siswa as $row): ?>
                      <?php echo (isset($f['n']) AND $f['r'] == $row['student_nis']) ? 
                      '<td>'.$row['student_full_name'].'</td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <tr>
                    <td>Nama Ibu Kandung</td>
                    <td>:</td>
                    <?php foreach ($siswa as $row): ?>
                      <?php echo (isset($f['n']) AND $f['r'] == $row['student_nis']) ?  
                      '<td>'.$row['student_name_of_mother'].'</td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <tr>
                    <td>Kelas</td>
                    <td>:</td>
                    <?php foreach ($siswa as $row): ?>
                      <?php echo (isset($f['n']) AND $f['r'] == $row['student_nis']) ? 
                      '<td>'.$row['class_name'].'</td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <?php if (majors()=='senior') { ?>
                  <tr>
                    <td>Program Keahlian</td>
                    <td>:</td>
                    <?php foreach ($siswa as $row): ?>
                      <?php echo (isset($f['n']) AND $f['r'] == $row['student_nis']) ? 
                      '<td>'.$row['majors_name'].'</td>' : '' ?> 
                    <?php endforeach; ?>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div><br><br>
					<div class="col-md-">
						<div class="box box-info box-solid" style="border: 1px solid #2ABB9B !important;">
						<div class="box-header backg with-border">
								<h3 class="box-title">Transaksi Terakhir</h3>
							</div><!-- /.box-header -->
							<hr>
							<div class="box-body">
								<table class="table table-striped" style="white-space: nowrap;">
									<tr class="info">
										<th>Pembayaran</th>
										<th>Tagihan</th>
										<th>Tanggal</th>
									</tr>
									<?php 
									foreach ($log as $key) :
									?>
									<tr>
                  <td>
											<?php echo ($key['bulan_bulan_id']!= NULL) ? $key['posmonth_name'].' - T.A '.$key['period_start_month'].'/'.$key['period_end_month'].' ('.$key['month_name'].')' : $key['posbebas_name'].' - T.A '.$key['period_start_bebas'].'/'.$key['period_end_bebas'] ?>
										</td>
										
										<td>
											<?php echo ($key['bulan_bulan_id']!= NULL) ? 'Rp. '. number_format($key['bulan_bill'], 0, ',', '.') : 'Rp. '. number_format($key['bebas_pay_bill'], 0, ',', '.') ?></td>

										<td>
											<?php echo pretty_date($key['log_trx_input_date'],'d F Y',false)  ?>
										</td>
									</tr>
								<?php endforeach ?>
								</table>
							</div>
						</div>
						</div>
        			</div>


    <div class="col-md-6">
    <!-- List Tagihan Pembayaran --> 
    <div class="box box-info box-solid" style="border: 1px solid #2ABB9B !important;">
        <div class="box-header backg with-border">
            <h3 class="box-title">Jenis Pembayaran</h3>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Bulanan</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Bebas</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="box-body table-responsive">
                            <table class="table table-hover table-responsive table-bordered" style="cursor: pointer;">
                                <thead class="table table-striped">
                                    <tr class="info">
                                        <th class="center">No.</th>
                                        <th class="center">Jenis Pembayaran</th>
                                        <th class="center">Total Tagihan</th>
                                        <?php foreach ($bulan as $key) : ?>
                                            <th><?php echo $key['month_name'] ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($student as $row) {
                                        $namePay = $row['pos_name'] . ' - T.A ' . $row['period_start'] . '/' . $row['period_end'];
                                        $unpaidFound = false;

                                        foreach ($bulan as $key) {
                                            if ($key['bulan_status'] != 1) {
                                                $unpaidFound = true;
                                                break;
                                            }
                                        }

                                        if (isset($f['n']) && $f['r'] == $row['student_nis'] && $unpaidFound) {
                                            ?>
                                            <tr data-toggle="collapse" style="color:
                                            <?php echo ($total == $pay) ? '#00E640' : 'red' ?>">

                                            <td><?php echo $i ?></td>
                                            <td>
                                              <?php echo $namePay ?>
                                            </td>
                                            <td>
                                              <?php echo 'Rp. ' . number_format($total - $pay, 0, ',', '.') ?>
                                            </td>
                                            
                                            <?php foreach ($bulan as $key) : ?>
                                            <td class="<?php echo ($key['bulan_status'] == 1) ? 'success' : 'danger' ?>">

												<?php if ($key['bulan_status'] == 1) : ?>
													<a class="status-lunas" onclick="showReceipt('<?php echo $row['student_nis']; ?>')">
														LUNAS <?php echo pretty_date($key['bulan_date_pay'], 'd/m/y', false); ?>
													</a>
												<?php else : ?>

													<a onclick="showReceipt('<?php echo $row['student_nis']; ?>')">
														<?php echo number_format($key['bulan_bill'], 0, ',', '.'); ?>
													</a>

												<?php endif; ?>
											</td>
                                                <?php endforeach ?>
                                            </tr>
                                            <?php
                                        }
                                        $i++;
                                    }
                                    ?>
                                </tbody>
								<tr class="f">
									<?php
									$totalTagihanPerBulan = $bulan_bill-$bulan_bill;
									?>
                                        <th class="text-end" colspan="3">Total Seluruhnya</th>
										<?php foreach ($bulan as $key) : ?>
										<td class="<?php echo ($key['bulan_status'] == 1) ? 'success' : 'danger' ?>">
											
											<?php if ($key['bulan_status'] == 1) : ?>
												<span class="status-lunas">
													LUNAS
												</span>
											<?php else : ?>
												<button class="btn btn-success" onclick="showReceipt('<?php echo $row['student_nis']; ?>', '<?php echo $row['student_full_name']; ?>', '<?php echo $key['month_name']; ?>', '<?php echo $totalTagihanPerBulan[$key['month_name']]; ?>')">

													<?php echo ' ' . number_format($key['bulan_bill'], 0, ',', '.'); ?>
												</button>
											<?php endif; ?>
										</td>
									<?php endforeach ?>
                                    </tr>
                            </table>
                        </div>
                    </div>
    
        <div class="tab-pane" id="tab_2">
			<!-- End List Tagihan Bulanan -->
			<!-- List Tagihan Lainnya (Bebas) -->
		<div class="box-body">
			<a href="" class="btn btn-info btn-xs"><i class="fa fa-refresh"></i> Refresh</a>
                      <p>
			<table class="table table-hover table-responsive table-bordered" style="white-space: nowrap;">
				<thead>
					<tr class="info">
						<th>No.</th>
						<th>Jenis Pembayaran</th>
						<th>Total Tagihan</th>
						<th>Dibayar</th>
						<th>Status</th>
						<th>Bayar</th>
                    </tr>
				</thead>
				<tbody>
				<?php
				$i =1;
					foreach ($bebas as $row):
						if ($f['n'] AND $f['r'] == $row['student_nis']) {
							$sisa = $row['bebas_bill']-$row['bebas_total_pay'];
				?>
					<tr class="<?php echo ($row['bebas_bill'] == $row['bebas_total_pay']) ? 'success' : 'danger' ?>">
						<td style="background-color: #fff !important;"><?php echo $i ?></td>
						<td style="background-color: #fff !important;"><?php echo $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'] ?></td>
						<td><?php echo 'Rp. ' . number_format($sisa, 0, ',', '.') ?></td>
						<td><?php echo 'Rp. ' . number_format($row['bebas_total_pay'], 0, ',', '.') ?></td>
						<td><a href="<?php echo site_url('manage/payout/payout_bebas/'. $row['payment_payment_id'].'/'.$row['student_student_id'].'/'.$row['bebas_id']) ?>" class="view-cicilan label <?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'label-success' : 'label-warning' ?>"><?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'Lunas' : 'Belum Lunas' ?></a></td>
						<td width="40" style="text-align:center">
						<a data-toggle="modal" class="btn btn-success btn-xs <?php echo ($row['bebas_bill']==$row['bebas_total_pay']) ? 'disabled' : '' ?>" title="Bayar" href="#addCicilan<?php echo $row['bebas_id'] ?>"><span class="fa fa-money"></span> Bayar</a>
						</td>
					</tr>

				<div class="modal fade" id="addCicilan<?php echo $row['bebas_id'] ?>" role="dialog">
					<div class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Tambah Pembayaran/Cicilan</h4>
							</div>
				<?php echo form_open('manage/payout/payout_bebas/', array('method'=>'post')); ?>

				<div class="modal-body">
					<input type="hidden" name="bebas_id" value="<?php echo $row['bebas_id'] ?>">
					<input type="hidden" name="student_nis" value="<?php echo $row['student_nis'] ?>">
					<input type="hidden" name="student_student_id" value="<?php echo $row['student_student_id'] ?>">
					<input type="hidden" name="payment_payment_id" value="<?php echo $row['payment_payment_id'] ?>">
				<div class="form-group">

				<label>Nama Pembayaran</label>
					<input class="form-control" readonly="" type="text" value="<?php echo $row['pos_name'].' - T.A '.$row['period_start'].'/'.$row['period_end'] ?>">
				</div>
					<div class="form-group">
						<label>Tanggal</label>
						<input class="form-control" readonly="" type="text" value="<?php echo pretty_date(date('Y-m-d'),'d F Y',false) ?>">
					</div>
						<div class="row">
							<div class="col-md-6">
								<label>Jumlah Bayar *</label>
									<input type="text" required="" name="bebas_pay_bill" class="form-control numeric" placeholder="Jumlah Bayar">
							</div>
							<div class="col-md-6">
								<label>Keterangan *</label>
									<input type="text" required="" name="bebas_pay_desc" class="form-control" placeholder="Keterangan">
							</div>
						</div>
						</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-success">Save</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</div>
						<?php echo form_close(); ?>
					</div>
				<?php 
			}
			$i++;
			endforeach; 
		?>				
			</tbody>
		</table> 
	</div>
</div>
<?php } ?>
</div>
</div>
</div>
</div>
</section><br><br>

<!-- Popup untuk tes tombol button -->
<div class="modal fade" id="receiptModal" tabindex="-1" role="dialog" aria-labelledby="receiptModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="receiptModalLabel">Struk Pembayaran</h4>
      </div>
      <div class="modal-body">
        <div id="receiptContent">
          <!--Tombol  -->
        </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-success" data-dismiss="modal">Print</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


    <!-- Modal untuk input kata sandi -->

    <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="passwordModalLabel">Masukkan Kata Sandi</h4>
          </div>
          <div class="modal-body">
            <div id="passwordError" style="color: red; display: none;">Kata sandi salah, coba lagi.</div>

            <form method="POST" action="" id="passwordForm">
              <div class="form-group">
                <label for="passwordInput">Kata Sandi:</label>

                <input type="password" class="form-control" id="passwordInput" name="password" placeholder="Masukkan kata sandi" autofocus required> 
                
            </div>

              <div class="container">
                <!-- Tombol Angka -->
                <button type="button" class="button number" value="1">1</button>
                <button type="button" class="button number" value="2">2</button>
                <button type="button" class="button number" value="3">3</button>
                &nbsp
                <button type="button" class="button letter" value="A">A</button>
                <button type="button" class="button letter" value="B">B</button>
                <button type="button" class="button letter" value="C">C</button>
                <button type="button" class="button letter" value="D">D</button>
                <button type="button" class="button letter" value="E">E</button>

                &nbsp 
                <button type="button" class="btn btn-primary" id="capslockButton">Caps Lock</button>

                <br>
                <button type="button" class="button number" value="4">4</button>
                <button type="button" class="button number" value="5">5</button>
                <button type="button" class="button number" value="6">6</button>
                &nbsp
                <button type="button" class="button letter" value="F">F</button>
                <button type="button" class="button letter" value="G">G</button>
                <button type="button" class="button letter" value="H">H</button>
                <button type="button" class="button letter" value="I">I</button>
                <button type="button" class="button letter" value="J">J</button>
                <br>
                <button type="button" class="button number" value="7">7</button>
                <button type="button" class="button number" value="8">8</button>
                <button type="button" class="button number" value="9">9</button>
                &nbsp
                <button type="button" class="button letter" value="K">K</button>
                <button type="button" class="button letter" value="L">L</button>
                <button type="button" class="button letter" value="M">M</button>
                <button type="button" class="button letter" value="N">N</button>
                <button type="button" class="button letter" value="O">O</button>
                <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <button type="button" class="button number" value="0">0</button>

                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
                <button type="button" class="button letter" value="P">P</button>
                <button type="button" class="button letter" value="Q">Q</button>
                <button type="button" class="button letter" value="R">R</button>
                <button type="button" class="button letter" value="S">S</button>
                <button type="button" class="button letter" value="T">T</button>
                <br>

                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <button type="button" class="button letter" value="U">U</button>
                <button type="button" class="button letter" value="V">V</button>
                <button type="button" class="button letter" value="W">W</button>
                <button type="button" class="button letter" value="X">X</button>
                <button type="button" class="button letter" value="Y">Y</button>
                <br>
                &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                <button type="button" class="button letter" value="Z">Z</button>

              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success"> <i class="fa fa-search"> Submit </i></button>
                <button type="reset" class="btn btn-danger"> <i class="fa fa-trash"> Delete </i></button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>


<script>
      document.getElementById('openPasswordModal').addEventListener('click', function() {
        $('#passwordModal').modal('show');
      });

    $(document).ready(function() {
    var capsLockActive = false; // Status awal Caps Lock nonaktif
    
    // Mengaktifkan/menonaktifkan Caps Lock saat tombol Capslock diklik
    $('#capslockButton').click(function() {
        capsLockActive = !capsLockActive; // Toggle capsLockActive
        $(this).toggleClass('active'); // Toggle kelas 'active' pada tombol Capslock
        updateCapsLockButtonStyle(); // Panggil fungsi untuk memperbarui gaya tombol Capslock
    });
    
    // Event handler untuk semua tombol huruf
    $(".button.letter").click(function() {
        var letter = $(this).val();
        var passwordInput = $("#passwordInput");
        var currentPassword = passwordInput.val();
        
        // Caps Lock aktif: periksa kapitalisasi saat menambah huruf
        if (capsLockActive) {
            passwordInput.val(currentPassword + letter.toUpperCase());
        } else {
            passwordInput.val(currentPassword + letter.toLowerCase());
        }
    });
    
    // Fungsi untuk memperbarui gaya tombol Capslock
    function updateCapsLockButtonStyle() {
        var capsLockButton = $('#capslockButton');
        if (capsLockActive) {
            capsLockButton.css('font-size', '20px'); // Ubah ukuran font saat Caps Lock aktif
            // Tambahan gaya lainnya saat Caps Lock aktif
        } else {
            capsLockButton.css('font-size', '16px'); // Kembalikan ukuran font saat Caps Lock nonaktif
            // Reset gaya tambahan lainnya saat Caps Lock nonaktif
        }
    }
});

      document.getElementById('submitPassword').addEventListener('click', function() {
        var enteredPassword = document.getElementById('passwordInput').value;
        var correctPassword = 'your_password'; // Ganti dengan kata sandi yang benar

        if (enteredPassword === correctPassword) {
          // Jika kata sandi benar, tutup modal dan lanjutkan pencarian
          $('#passwordModal').modal('hide');
          document.querySelector('form').submit();
        } else {
          // Jika kata sandi salah, tampilkan pesan kesalahan
          document.getElementById('passwordError').style.display = 'block';
        }
      });
    </script>

	<style>

      #passwordError {
        margin-top: 10px;
      }
	  .status-lunas {
			pointer-events: none;
			color: yellowgreen; /* Ubah warna teks jika diinginkan */
		}
    </style>
	

	<script>
	// isi popup
	function showReceipt(nis, siswa, month, totalTagihanPerBulan, barcodeUrl) {
	var receiptContent = `
		<p>NIS: ${nis}</p>
		<p>Nama: ${siswa}</p>
		<p>Bulan: ${month}</p>
		<p>Jumlah: Rp. ${totalTagihanPerBulan}</p>
		<p>Barcode:</p>
		<img src="${barcodeUrl}" alt="Barcode" class="img-thumbnail img-responsive">
	`;
	document.getElementById('receiptContent').innerHTML = receiptContent;
	$('#receiptModal').modal('show');
	}
	</script>

<style>
    .f {
      background-color: #d3d3d3; /* Warna abu-abu */
    }
    .text-end {
      text-align: right;
    }
  </style>

<style>
        .container {
            text-align: left;
        }
        .button {
            padding: 10px 15px;
            margin: 5px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 13px;
        }
        .number {
            background-color: #4CAF50;
            color: white;
        }
        .letter {
            background-color: #2196F3;
            color: white;
        }
    </style>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

